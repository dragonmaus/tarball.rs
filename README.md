# tarball

A complete, organised command-line solution for archiving and (optionally)
compressing directory trees.

## Links:
- [Crates.io](https://crates.io/crates/tarball)
- [Documentation](https://docs.rs/tarball/)
- [Repository](https://nest.pijul.com/dragonmaus/tarball)
- [Repository mirror (Darcs Hub)](https://hub.darcs.net/dragonmaus/tarball.rs)
- [Repository mirror (GitLab)](https://gitlab.com/dragonmaus/tarball.rs)
- [Repository mirror (GitHub)](https://github.com/dragonmaus/tarball.rs)
